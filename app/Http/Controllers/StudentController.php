<?php

namespace App\Http\Controllers;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Models\Student;

class StudentController extends Controller
{
    function addData(Request $req){

        
        // $student = new Student;

        // $student->name = $req->name;
        // $student->email = $req->email;
        // $student->save();

        // $courseIds = $req->input('courses', []);
        // $student->courses()->attach($courseIds);

        // return redirect()->route('std_list');

// create PDO connection
$pdo = $this->connectDB();

// get data from the request
    $name = $req->name;
    $email = $req->email;
    $courseIds = $req->input('courses', []);

    $sql = "INSERT INTO `students` (`name`, `email`, `created_at`) VALUES (:name, :email, now())";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':email', $email);
    $stmt->execute();

// store the last inserted student_id
    $studentId = $pdo->lastInsertId();

// attach courses with student_id into student_courses table
    if (!empty($courseIds)) {
        $sql = "INSERT INTO `student_courses` (`student_id`, `course_id`) VALUES (:student_id, :course_id)";
        $stmt = $pdo->prepare($sql);
        foreach ($courseIds as $courseId) {
            $stmt->bindParam(':student_id', $studentId);
            $stmt->bindParam(':course_id', $courseId);
            $stmt->execute();
        }
    }

    header('Location: sShowData'); 
    exit();
   
}


function index(){
$students = Student::with('courses')->get();
return view('s_list', compact('students') );
}

public function add()
{
    $courses = Course::all();
    return view('s_add', compact('courses'));
}

public function addCourse(Request $req)
{
    $std = Student::find($req->id);
    $courses = Course::all();
    return view('s_addCourse', compact('courses', 'std'));
}

public function editCourse(Request $req)
{
    $edit = Student::find($req->id);
    $courses = Course::all();
    $checkedCourses = $edit->courses->pluck('id')->toArray();
    return view('s_editCourse', compact('edit', 'courses', 'checkedCourses'));
}


function editCourseStore(Request $req){

        
    $student = Student::find($req->id);
    $student->courses()->detach();
    $courseIds = $req->input('courses', []);
    $student->courses()->attach($courseIds);

    return redirect()->route('std_list');

}

//data insert into pivot table
public function addCourseStore(Request $req)
{
   $student = Student::find($req->id);
   $courseIds = $req->input('courses', []);
   
   // Attach the selected courses to the student
   $student->courses()->attach($courseIds);

   return redirect()->route('std_list');
}

public function show(Request $req)
{
    // $std = Student::find($req->id);
    // $courses = $std->courses;
    // return view('std_show', compact('std', 'courses'));
    $pdo = $this->connectDB();

    $id = $req->id;

    $sql = "SELECT * FROM `students` WHERE `id` = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    $stmt->execute();
    $std = $stmt->fetch(\PDO::FETCH_ASSOC);

    // Fetch associated courses for student
    $sql = "
        SELECT c.*
        FROM courses c
        INNER JOIN student_courses sc ON c.id = sc.course_id
        WHERE sc.student_id = :id
    ";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    $stmt->execute();
    $courses = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $pdo = null;

    return view('std_show', compact('std', 'courses'));
}

public function delete(Request $req)
{
  
    Student::find($req->id)->delete();
    return redirect()->route('std_list');
}

public function edit(Request $req)
{
  
    // $edit = Student::find($req->id);
    // $courses = Course::all();
    // $checkedCourses = $edit->courses->pluck('id')->toArray();
    // return view('s_edit', compact('edit', 'courses', 'checkedCourses'));

    $pdo = $this->connectDB();
    $id = $req->id;

    $stmt = $pdo->prepare("SELECT * FROM students WHERE id = :id");
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    $stmt->execute();
    $edit = $stmt->fetch(\PDO::FETCH_ASSOC);

    // fetch all courses
    $stmt = $pdo->prepare("SELECT * FROM courses");
    $stmt->execute();
    $courses = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    // fetch checked courses for the student
    $stmt = $pdo->prepare("SELECT course_id FROM student_courses WHERE student_id = :id");
    $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
    $stmt->execute();
    $checkedCourses = array_column($stmt->fetchAll(\PDO::FETCH_ASSOC), 'course_id');

    $pdo = null;
    return view('s_edit', compact('edit', 'courses', 'checkedCourses'));
}

function update(Request $req){
    
    // $student = Student::find($req->id);
    // $student->name = $req->name;
    // $student->email = $req->email;
    // $student->update();
    // $courseIds = $req->input('courses', []);
    // $student->courses()->sync($courseIds);

    // return redirect()->route('std_list');


    $pdo = $this->connectDB();
// get data from the request
    $id = $req->id;
    $name = $req->name;
    $email = $req->email;
    $courseIds = $req->input('courses', []);

// update student data in the students table
    $sql = "UPDATE `students` SET `name` = :name, `email` = :email, `updated_at` = now() WHERE `id` = :id";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':id', $id);
    $stmt->execute();

// detach existing entries from student_courses table
      $sql = "DELETE FROM `student_courses` WHERE `student_id` = :id";
      $stmt = $pdo->prepare($sql);
      $stmt->bindParam(':id', $id);
      $stmt->execute();

// attach courses into the student_courses table
      if (!empty($courseIds)) {
          $sql = "INSERT INTO `student_courses` (`student_id`, `course_id`) VALUES (:student_id, :course_id)";
          $stmt = $pdo->prepare($sql);
          foreach ($courseIds as $courseId) {
              $stmt->bindParam(':student_id', $id);
              $stmt->bindParam(':course_id', $courseId);
              $stmt->execute();
          }
      }

      header('Location: sShowData'); 
      exit();

}

   public function connectDB()
     {
       $dbs = "mysql:host=localhost;dbname=course";
       $username = "root";
       $password = "";
       try {
        $pdo = new \PDO($dbs, $username, $password);
        return $pdo;
       } catch (\PDOException $e) {
        die("error: " . $e->getMessage());
       }
     }

}
