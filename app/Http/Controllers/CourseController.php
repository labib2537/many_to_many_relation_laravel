<?php

namespace App\Http\Controllers;
use App\Models\Course;
use App\Models\Student;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function list()
    {
        $courses = Course::all();
        return view('list', compact('courses'));
    }
    public function add()
    {
        return view('add');
    }

    public function store(Request $req)
    {
        $course = new Course;
        $course->course_name = $req->cname;
        $course->course_code = $req->code;
        $course->save();
        return redirect()->route('course_list');
    }

    public function edit(Request $req)
    {
        $course = Course::find($req->id);
        return view('edit', compact('course'));
    }

    public function update(Request $req)
    {
        $course = Course::find($req->id);
        $course->course_name = $req->cname;
        $course->course_code = $req->code;
        $course->update();
        return redirect()->route('course_list');
    }

    public function delete(Request $req)
    {
        Course::find($req->id)->delete();
        return redirect()->route('course_list');
    }

    public function show(Request $req)
    {
        $course = Course::find($req->id);
        $students = $course->students;
        return view('show', compact('course', 'students'));
    }
}
