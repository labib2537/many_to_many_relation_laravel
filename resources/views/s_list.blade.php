
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
     <a class="btn btn-primary my-3" href="{{route('std_add')}}">Add Student</a>   
     <a class="btn btn-warning my-3" href="{{route('home')}}">Home</a>   
    <table class="table">
  <thead>
    <tr>
    <th>#</th>
    <th>Name</th>
    <th>Email</th>
    <th>Action</th>
  
  
    </tr>
  </thead>
  <tbody>
  @foreach ($students as $key=>$student)
    <tr>
    <td>{{++$key}}</td>
    <td>{{$student->name}}</td>
    <td>{{$student->email}}</td>
    <td>
    <div class="d-flex">
      @if(count($student->courses)>0)
      <form action="{{route('std_edit_course')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$student->id}}">
    <button class="btn btn-secondary mx-1" type="submit">Edit Course</button> 
</form> 
      @else
      <form action="{{route('std_add_course')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$student->id}}">
    <button class="btn btn-dark mx-1" type="submit">Add Course</button> 
</form> 
      @endif
    
    <form action="{{route('std_show')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$student->id}}">
    <button class="btn btn-primary mx-1" type="submit">View</button> 
</form>   
<form action="{{route('std_edit')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$student->id}}">
    <button class="btn btn-success mx-1" >Edit</button> 
</form>
      
<form action="{{route('std_delete')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$student->id}}">
    <button class="btn btn-danger mx-1" onclick="return confirm('Are you sure?')" >Delete</button> 
   
</form>   
</div>
      </td>
     
      
    </tr>
    @endforeach
   
  </tbody>
</table>
    </div>
  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>