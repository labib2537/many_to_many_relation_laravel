

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
     <a class="btn btn-primary my-3" href="{{route('std_list')}}">Back To Student List</a>   
    <table class="table">
  <thead>
    <tr>
    <th>#</th>
    <th>Name</th>
    <th>Email</th>
    <th>Courses Taken</th>
  
  
    </tr>
  </thead>
  <tbody>

 {{-- <tr>
    <td>{{$std->id}}</td>
    <td>{{$std->name}}</td>
    <td>{{$std->email}}</td>
    <td>
        @if(count($courses)>0)
        <ul>
        @foreach($courses as $course)
      
            <li>{{$course->course_name}}</li>
           
        @endforeach
        </ul>
        @else
        <span class="text-danger">No Courses Taken Yet!</span>
        @endif
    </td>
    </tr>  --}}

    <tr>
    <td>{{$std['id']}}</td>
    <td>{{$std['name']}}</td>
    <td>{{$std['email']}}</td>
    <td>
        @if(count($courses)>0)
        <ul>
        @foreach($courses as $course)
      
            <li>{{$course['course_name']}}</li>
           
        @endforeach
        </ul>
        @else
        <span class="text-danger">No Courses Taken Yet!</span>
        @endif
    </td>
    </tr>
 
   
  </tbody>
</table>
    </div>
  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>