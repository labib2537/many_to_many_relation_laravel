

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
     <a class="btn btn-primary my-3" href="{{route('course_insert')}}">Add Course</a>   
     <a class="btn btn-warning my-3" href="{{route('home')}}">Home</a>   
    <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Course Name</th>
      <th scope="col">Course Code</th>
      <th scope="col">Action</th>
  
  
    </tr>
  </thead>
  <tbody>
    @foreach($courses as $key=>$course)
    <tr>
      <th>{{++$key}}</th>
      <td>{{$course->course_name}}</td>
      <td>{{$course->course_code}}</td>
      <td>
      <div class="d-flex">
      <form action="{{route('course_show')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$course->id}}">
    <button class="btn btn-dark mx-1" >View</button> 
</form>
    <form action="{{route('course_edit')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$course->id}}">
    <button class="btn btn-success mx-1" >Edit</button> 
</form>
      
<form action="{{route('course_delete')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$course->id}}">
    <button class="btn btn-danger mx-1" onclick="return confirm('Are you sure?')">Delete</button> 
</form>   
</div>
      </td>
     
      
    </tr>
    @endforeach
   
  </tbody>
</table>
    </div>
  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>