
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
    <h5 class="my-3">Edit Courses</h5>
    <p><b>Student Name:</b> {{$edit->name}}</p>
    <form action="{{route('std_edit_course_store')}}" method="post"> 
        @csrf
        <input type="hidden" name="id" value="{{$edit->id}}">
  <div class="mb-3">
  <div class="form-group">
        <label>Courses:</label><br>
        @foreach($courses as $course)
            <div class="form-check">
                <input type="checkbox" name="courses[]" value="{{ $course->id }}" class="form-check-input" 
                @if(in_array($course->id, $checkedCourses)) checked @endif>
                <label class="form-check-label">{{ $course->course_name }}</label>
            </div>
        @endforeach
    </div>
</div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>