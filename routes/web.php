<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\StudentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', function () {
    return view('welcome');
})->name('home');


Route::get('/list', [CourseController::class, 'list'])->name('course_list');
Route::get('/add', [CourseController::class, 'add'])->name('course_insert');
Route::post('/store', [CourseController::class, 'store'])->name('course_store');
Route::post('/edit', [CourseController::class, 'edit'])->name('course_edit');
Route::post('/update', [CourseController::class, 'update'])->name('course_update');
Route::post('/delete', [CourseController::class, 'delete'])->name('course_delete');
Route::post('/showCourse',[CourseController::class, 'show'])->name('course_show');

Route::post('sAddData',[StudentController::class, 'addData']);
Route::get('sShowData',[StudentController::class, 'index'])->name('std_list');
Route::get('/s-add',[StudentController::class, 'add'])->name('std_add');
Route::post('/show',[StudentController::class, 'show'])->name('std_show');
Route::post('/s_delete',[StudentController::class, 'delete'])->name('std_delete');
Route::post('/s_edit', [StudentController::class, 'edit'])->name('std_edit');
Route::post('/s_update', [StudentController::class, 'update'])->name('std_update');
Route::post('/s_addCourse', [StudentController::class, 'addCourse'])->name('std_add_course');
Route::post('/s_addCourseStore', [StudentController::class, 'addCourseStore'])->name('std_add_course_store');
Route::post('/s_editCourse', [StudentController::class, 'editCourse'])->name('std_edit_course');
Route::post('/s_editCourseStore', [StudentController::class, 'editCourseStore'])->name('std_edit_course_store');
